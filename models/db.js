const mongoose = require('mongoose')
const url = 'mongodb://localhost:27017/EmployeeDB'

mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
  if (!err) {
    console.log('Connection SUCCESS')
  } else {
    console.log("error" + err)
  }
})

// inport model for useable to controller
require('./employee.model')
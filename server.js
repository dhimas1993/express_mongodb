require('./models/db')

const express = require('express')
const employeeController = require('./controller/employeeController')
const path = require('path')
const handlebars = require('express-handlebars')
const bodyparser = require('body-parser')

const app = express()
const port = 3000

//library for parsong props from controller
app.use(bodyparser.urlencoded({
  extended: true
}))
//convert to json
app.use(bodyparser.json())

app.set('views', path.join(__dirname, '/views/'))
app.engine('handlebars',
  handlebars({
    extname: 'handlebars',
    defaultLayout: 'mainLayout', layoutsDir: __dirname + '/views/layout/'
  }))
app.set('view engine', 'handlebars')

app.listen(port, () => {
  console.log(`Server Running at port ${port}`)
})

app.use('/employee', employeeController)